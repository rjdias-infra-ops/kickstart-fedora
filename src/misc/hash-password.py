#!/usr/bin/env python3
import getpass
import crypt

p1 = getpass.getpass("New password: ");
p2 = getpass.getpass("Retype new password: ");

if (p1 == p2):
  print("SHA512 salted hash:\n", crypt.crypt(p2, crypt.mksalt(crypt.METHOD_SHA512)), sep='');
else:
  print ("Sorry, passwords do not match.");

