# Fedora Kickstart deployment
This repository has examples for a Fedora automated kickstart installation.


## Contents:
* Python [script][hash-password] to hash and salt a password.
* Bash [script][bootstrap-generic] to bootstrap a VM with Kickstart using libvirt.
* Kickstart file for Fedora [Server Minimal][minimal-ks] and Fedora [Gnome Workstation][gnome-ks].

## How to use:
* Change the parameters on the Kickstart files [[1][minimal-ks],[2][gnome-ks]], such as:
  * Packages, username, password, SSH keys, keyboard layout, language, timezone...

* Change the parameters on [bootstrap-generic][bootstrap-generic], such as:
  * RAM, vCPUs, Storage (`$disk_dir`)...

* Uncomment the installation source on [bootstrap-generic][bootstrap-generic] accordingly:
  * HTTPS kickstart file:  
    (copy the Kickstart file to the HTTP server)
    ```bash
    extra_args="--extra-args ks=https://example.com/ks.cfg"
    ```

  * Local kickstart file:
    ```bash
    extra_args="--initrd-inject=./minimal-ks.cfg --extra-args ks=file:/minimal-ks.cfg"
    ```

* Run the [bootstrap-generic][bootstrap-generic] script.


## Extras:
* If you are deploying this on bare metal:
  * Remove the `@guest-desktop-agents` or `@guest-agents` package from the Kickstart file.
  * Download and flash a USB with the [Everything Netinst ISO][everything-iso] and boot it.
  * At the boot screen select `Install Fedora` and press **TAB**.
  * Insert `inst.ks=https://example.com/gnome-ks.cfg` before the `quiet` option.
  * Press `Ctrl-x` to boot.


## References
[Kickstart Documentation (latest)](https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html)

[bootstrap-generic]: src/bootstrap-generic.sh
[hash-password]: src/misc/hash-password.py
[minimal-ks]: src/minimal-ks.cfg
[gnome-ks]: src/gnome-ks.cfg
[everything-iso]: https://download.fedoraproject.org/pub/fedora/linux/releases/30/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-30-1.2.iso

