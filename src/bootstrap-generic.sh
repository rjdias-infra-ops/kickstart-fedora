#!/bin/bash
set -eux;

# Edit variables to your preferences
rand=$(cat /dev/urandom | tr -dc 'a-z' | fold -w 6 | head -n 1)
instance="fedora-$rand"

# Storage
disk_dir="$(pwd)"
disk_size="16"
disk_path="${disk_dir}/${instance}"

# vCPU & RAM
ram="4096"
vcpus="4"
vcpumodel="host"

# Networking
net="--network network=default"
#net="--network bridge=bridge0"

# Distribution tree installation source
#source="--location=./Fedora-Everything-netinst-x86_64-30-1.2.iso"
#source="--location=https://example.com/fedora/releases/30/Everything/x86_64/os/"
source="--location=https://download.fedoraproject.org/pub/fedora/linux/releases/30/Everything/x86_64/os/"

# Anaconda kickstart file
#extra_args="--extra-args ks=https://example.com/ks.cfg"
#extra_args="--initrd-inject=./minimal-ks.cfg --extra-args ks=file:/minimal-ks.cfg"

# Specifies the graphical display configuration
display="--graphics spice --video=qxl --channel spicevmc --noautoconsole"

#display="--graphics none --noautoconsole"
#extra_args+=" --extra-args console=ttyS0,115200n8,targe.type=serial --console pty,target_type=serial"

# Start instance...
sudo virt-install \
--name $instance \
--disk path=$disk_path.qcow2,format=qcow2,size=$disk_size \
--memory $ram \
--cpu $vcpumodel \
--vcpus $vcpus \
--os-variant fedora30 \
--boot uefi \
$net \
$source \
$display \
$extra_args \
;

